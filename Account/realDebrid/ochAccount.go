package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"time"

	"gitlab.com/dimakow86/ochTorrentLoader/Torrent"
	"gitlab.com/dimakow86/ochTorrentLoader/util"
)

type credentials struct {
	Client_id       string
	Client_secret   string
	Device_code     string
	Current_token   string
	Refresh_token   string
	User_code       string
	Time_to_refresh int64
}

type RealDebridAccount struct {
	Authorized            bool
	Credentials           credentials
	torrentRequestChannel chan Torrent.TorrentRequest
}

type initialResponse struct {
	DeviceCode            string `json:"device_code"`
	UserCode              string `json:"user_code"`
	Interval              int    `json:"interval"`
	ExpiresIn             int    `json:"expires_in"`
	VerificationURL       string `json:"verification_url"`
	DirectVerificationURL string `json:"direct_verification_url"`
}

type credentialsResponse struct {
	ClientID     string `json:"client_id"`
	ClientSecret string `json:"client_secret"`
}

type tokenResponse struct {
	AccessToken  string `json:"access_token"`
	ExpiresIn    int    `json:"expires_in"`
	RefreshToken string `json:"refresh_token"`
	TokenType    string `json:"token_type"`
}

func (r *RealDebridAccount) Init() (err error) {
	err = r.loadEntity()
	if err != nil {
		return
	}

	r.torrentRequestChannel = make(chan Torrent.TorrentRequest)
	go r.workOnTorrentRequestChannel()

	if !r.Authorized {
		err = r.authorize()
	} else {
		fmt.Println("Already authorized, skipping authorization")
		fmt.Println("Checking if token is still valid")
		if r.Credentials.Time_to_refresh < time.Now().UTC().Unix() {
			fmt.Println("Time to get a new token!")
			err = r.getRefreshToken()
		} else {
			fmt.Println("Token is still valid!")
		}
	}
	return
}

func (r *RealDebridAccount) workOnTorrentRequestChannel() {
	for {
		select {
		case req := <-r.torrentRequestChannel:
			if r.Credentials.Time_to_refresh-60 < time.Now().UTC().Unix() {
				err := r.getRefreshToken()
				if err != nil {
					fmt.Println("Could not refresh token")
					os.Exit(1)
				}
			}
			tt := req.TorrentType
			switch tt {
			case "file":
				fmt.Println("File")
			case "magnet":
				fmt.Println("Magnet")
			}
		}
	}
}

func (r *RealDebridAccount) getActiveTorrents() (torrents int, err error) {
	var req *http.Request
	req, err = http.NewRequest("GET", "https://api.real-debrid.com/oauth/v2/torrents/activeCount", nil)
	req.Header.Add("Authorization", "Bearer "+r.Credentials.Current_token)
	if err != nil {
		return
	}

	client := &http.Client{}

	var resp *http.Response
	resp, err = client.Do(req)
	if err != nil {
		return
	}

	if resp.StatusCode != 200 {
		fmt.Println("Received HTTP not 200")
		return
	}
	defer resp.Body.Close()

	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("Error: %v", err)
		return
	}

	var tr tokenResponse
	json.Unmarshal(bodyBytes, &tr)
	if err != nil {
		fmt.Println("Error: %v", err)
		return
	}

	return
}

func (r *RealDebridAccount) authorize() (err error) {

	if !r.sendInitialRequest() {
		return errors.New("Error happened during intialRequest")
	}

	if !r.getClientSecret() {
		return errors.New("Could not get ClientId and ClientSecret")
	}

	err = r.getAuthorizationToken()
	if err != nil {
		return
	}
	r.saveEntity()

	return

	//fmt.Println("%+v", r)

}

func (r *RealDebridAccount) getAuthorizationToken() (err error) {
	bodyString := bytes.NewBufferString("client_id=" + r.Credentials.Client_id + "&client_secret=" + r.Credentials.Client_secret + "&code=" + r.Credentials.Device_code + "&grant_type=http://oauth.net/grant_type/device/1.0")
	resp, err := http.Post("https://api.real-debrid.com/oauth/v2/token", "application/x-www-form-urlencoded", bodyString)
	if err != nil {
		return
	}

	if resp.StatusCode != 200 {
		fmt.Println("Received HTTP not 200")
		return
	}
	defer resp.Body.Close()

	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("Error: %v", err)
		return
	}

	var tr tokenResponse
	json.Unmarshal(bodyBytes, &tr)
	if err != nil {
		fmt.Println("Error: %v", err)
		return
	}

	r.Credentials.Current_token = tr.AccessToken
	r.Credentials.Refresh_token = tr.RefreshToken
	r.Credentials.Time_to_refresh = time.Now().UTC().Unix() + int64(tr.ExpiresIn)

	r.saveEntity()

	return

}

func (r *RealDebridAccount) getRefreshToken() (err error) {

	bodyString := bytes.NewBufferString("client_id=" + r.Credentials.Client_id + "&client_secret=" + r.Credentials.Client_secret + "&code=" + r.Credentials.Refresh_token + "&grant_type=http://oauth.net/grant_type/device/1.0")
	resp, err := http.Post("https://api.real-debrid.com/oauth/v2/token", "application/x-www-form-urlencoded", bodyString)
	if err != nil {
		return
	}
	fmt.Println("Getting")

	if resp.StatusCode != 200 {
		fmt.Println("Received HTTP not 200")
		return
	}
	defer resp.Body.Close()

	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("Error: %v", err)
		return
	}

	var tr tokenResponse
	json.Unmarshal(bodyBytes, &tr)
	if err != nil {
		fmt.Println("Error: %v", err)
		return
	}

	fmt.Println("%v", tr)

	r.Credentials.Current_token = tr.AccessToken
	r.Credentials.Refresh_token = tr.RefreshToken
	r.Credentials.Time_to_refresh = time.Now().UTC().Unix() + int64(tr.ExpiresIn)

	r.saveEntity()

	return

}

func (r *RealDebridAccount) getClientSecret() bool {
	fmt.Println("Please use following code on real-debrid.com/device")
	fmt.Println(r.Credentials.User_code)
	fmt.Println()
	fmt.Println("Checking for 120sec...")

	ticker := time.NewTicker(5 * time.Second)

	for {
		select {
		case <-ticker.C:
			fmt.Println("Checking...")
			if r.getCredentialsEndpoint() {
				r.Authorized = true
				r.saveEntity()
				return true
			}
		case <-time.After(120 * time.Second):
			fmt.Println("Timed out 120sec")
			ticker.Stop()
			return false
		}
	}
	return false
}

func (r *RealDebridAccount) sendInitialRequest() bool {

	resp, err := http.Get("https://api.real-debrid.com/oauth/v2/device/code?client_id=X245A4XAIBGVM&new_credentials=yes")
	if err != nil {
		fmt.Println("Cannot reach real debrid api")
		return false
	}
	if resp.StatusCode != 200 {
		fmt.Println("Received HTTP not 200")
		return false
	}
	defer resp.Body.Close()

	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("Error: %v", err)
		return false
	}

	var ir initialResponse
	json.Unmarshal(bodyBytes, &ir)
	if err != nil {
		fmt.Println("Error: %v", err)
		return false
	}

	//fmt.Println("%+v", ir)

	r.Credentials.Device_code = ir.DeviceCode
	r.Credentials.User_code = ir.UserCode

	return true

}

func (r *RealDebridAccount) getCredentialsEndpoint() bool {
	resp, err := http.Get("https://api.real-debrid.com/oauth/v2/device/credentials?client_id=X245A4XAIBGVM&code=" + r.Credentials.Device_code)
	if err != nil {
		fmt.Println("Cannot reach real debrid api")
		return false
	}
	if resp.StatusCode != 200 {
		fmt.Println("Received HTTP not 200")
		return false
	}
	defer resp.Body.Close()

	bodyBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("Error: %v", err)
		return false
	}

	var cr credentialsResponse
	json.Unmarshal(bodyBytes, &cr)
	if err != nil {
		fmt.Println("Error: %v", err)
		return false
	}
	fmt.Println("%+v", cr)

	r.Credentials.Client_id = cr.ClientID
	r.Credentials.Client_secret = cr.ClientSecret

	return true

}

func (r *RealDebridAccount) saveEntity() (err error) {
	var b []byte
	//fmt.Println("%+v", r)
	b, err = json.Marshal(&r)
	if err != nil {
		fmt.Println(err)
		return
	}
	return util.SaveSettings(b)
}

func (r *RealDebridAccount) loadEntity() (err error) {
	var b []byte
	b, _ = util.LoadSettings()
	if b == nil {
		return
	}
	//	fmt.Println(string(b))
	//	t := new(RealDebridAccount)
	//	fmt.Println("%+v", t)
	err = json.Unmarshal(b, r)

	return

}

var Account RealDebridAccount
