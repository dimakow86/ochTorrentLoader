package main

import (
	"fmt"
	"os"
	"plugin"
	"sync"

	"gitlab.com/dimakow86/ochTorrentLoader/Account"
	"gitlab.com/dimakow86/ochTorrentLoader/Torrent"
)

func main() {

	plug, err := plugin.Open("./plugins/Account/realDebrid.so")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	symAccount, err := plug.Lookup("Account")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	var a Account.OchAccount
	a, ok := symAccount.(Account.OchAccount)
	if !ok {
		fmt.Println("unexpected type from module symbol")
		os.Exit(1)
	}

	err = a.Init()
	if err != nil {
		fmt.Println("Error while initing Account!")
		fmt.Println(err)
	} else {
		fmt.Println("Account Initing completed, going to wait for torrents")
	}

	var w Torrent.Watcher

	go w.LookForNewTorrents()

	var wg sync.WaitGroup
	wg.Add(1)
	wg.Wait()
}
