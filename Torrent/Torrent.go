package Torrent

//https://github.com/swatkat/gotrntmetainfoparser/blob/master/gotrntmetainfoparser.go
import (
	"bytes"
	"crypto/sha1"
	"os"
	"path/filepath"

	bencode "github.com/jackpal/bencode-go"
)

// Structs into which torrent metafile is
// parsed and stored into.
type FileDict struct {
	Length int64    "length"
	Path   []string "path"
	Md5sum string   "md5sum"
}

type InfoDict struct {
	FileDuration []int64 "file-duration"
	FileMedia    []int64 "file-media"
	// Single file
	Name   string "name"
	Length int64  "length"
	Md5sum string "md5sum"
	// Multiple files
	Files       []FileDict "files"
	PieceLength int64      "piece length"
	Pieces      string     "pieces"
	Private     int64      "private"
}

type TorrentMetaInfo struct {
	Info         InfoDict   "info"
	InfoHash     string     "info hash"
	Announce     string     "announce"
	AnnounceList [][]string "announce-list"
	CreationDate int64      "creation date"
	Comment      string     "comment"
	CreatedBy    string     "created by"
	Encoding     string     "encoding"
}

// Open .torrent file, un-bencode it and load them into MetaInfo struct.
func (metaInfo *TorrentMetaInfo) ReadTorrentMetaInfoFile(fileNameWithPath string) bool {
	// Check exntension.
	if fileExt := filepath.Ext(fileNameWithPath); fileExt != ".torrent" {
		return false
	}

	// Open file now.
	file, er := os.Open(fileNameWithPath)
	if er != nil {
		return false
	}
	defer file.Close()

	// Decode bencoded metainfo file.
	fileMetaData, er := bencode.Decode(file)
	if er != nil {
		return false
	}

	// fileMetaData is map of maps of... maps. Get top level map.
	metaInfoMap, ok := fileMetaData.(map[string]interface{})
	if !ok {
		return false
	}

	// Enumerate through child maps.
	var bytesBuf bytes.Buffer
	for mapKey, mapVal := range metaInfoMap {
		switch mapKey {
		case "info":
			if er = bencode.Marshal(&bytesBuf, mapVal); er != nil {
				return false
			}

			infoHash := sha1.New()
			infoHash.Write(bytesBuf.Bytes())
			metaInfo.InfoHash = string(infoHash.Sum(nil))

			if er = bencode.Unmarshal(&bytesBuf, &metaInfo.Info); er != nil {
				return false
			}

		case "announce-list":
			if er = bencode.Marshal(&bytesBuf, mapVal); er != nil {
				return false
			}
			if er = bencode.Unmarshal(&bytesBuf, &metaInfo.AnnounceList); er != nil {
				return false
			}

		case "announce":
			metaInfo.Announce = mapVal.(string)

		case "creation date":
			metaInfo.CreationDate = mapVal.(int64)

		case "comment":
			metaInfo.Comment = mapVal.(string)

		case "created by":
			metaInfo.CreatedBy = mapVal.(string)

		case "encoding":
			metaInfo.Encoding = mapVal.(string)
		}
	}

	return true
}
