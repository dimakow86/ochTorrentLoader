package Torrent

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"time"

	"gitlab.com/dimakow86/ochTorrentLoader/util"
)

type Watcher struct {
}

func (w *Watcher) LookForNewTorrents() {
	ticker := time.NewTicker(2 * time.Second)
	for {
		select {
		case <-ticker.C:
			fmt.Println("Checking for new torrents")
			w.checkDirectory()
		}
	}
}

func (w *Watcher) checkDirectory() {
	dir, err := filepath.Abs(filepath.Dir(os.Args[0]))
	if err != nil {
		log.Fatal(err)
	}

	torrentDir := filepath.Join(dir, "/torrents")

	files, err := ioutil.ReadDir(torrentDir)
	if err != nil {
		log.Fatal(err)
	}

	for _, file := range files {
		if filepath.Ext(file.Name()) == ".torrent" {
			torrentFile := filepath.Join(torrentDir, file.Name())
			t := new(TorrentMetaInfo)
			if t.ReadTorrentMetaInfoFile(torrentFile) {
				key := fmt.Sprintf("%X", t.InfoHash)
				fmt.Printf("%v\n", key)
				util.WriteToBucket([]byte("torrents"), []byte(key), nil)
			}
		}
	}
}
