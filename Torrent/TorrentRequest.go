package Torrent


type TorrentRequest struct{
	TorrentType 	string
	Link			string //File location or magnet link
	TorrentResponseChannel chan string // HTTP Link
} 