package util

import "fmt"
import "io/ioutil"
import "os"

func SaveSettings(bytes []byte) (err error) {
	var jsonFile *os.File
	jsonFile, err = os.Create("./settings.json")

	if err != nil {
		fmt.Println(err)
		panic(err)
	}
	defer jsonFile.Close()

	jsonFile.Write(bytes)
	jsonFile.Close()
	fmt.Println("Settings written to ", jsonFile.Name())

	return
}
func LoadSettings() (bytes []byte, err error) {
	bytes, err = ioutil.ReadFile("./settings.json")

	if err != nil {
		fmt.Println("Settings.json not found")
		return
	}

	fmt.Println("Settings read from settings.json")

	return
}
