package util

import (
	"fmt"
	"log"

	bolt "github.com/coreos/bbolt"
)

var torrents = []byte("torrents")

var db *bolt.DB

func init() {
	var err error
	db, err = bolt.Open("./settings/bolt.db", 0644, nil)
	if err != nil {
		log.Fatal(err)
	}
}

func WriteToBucket(bucket []byte, key []byte, value []byte) (err error) {
	// store some data

	err = db.Update(func(tx *bolt.Tx) error {
		bucket, err := tx.CreateBucketIfNotExists(bucket)
		if err != nil {
			return err
		}

		err = bucket.Put(key, value)
		if err != nil {
			return err
		}
		return nil
	})

	if err != nil {
		log.Fatal(err)
	}
	return
}

func GetFromBucket(bucket string, key []byte) (value []byte, err error) {

	// retrieve the data
	err = db.View(func(tx *bolt.Tx) error {
		bucket := tx.Bucket(torrents)
		if bucket == nil {
			return fmt.Errorf("Bucket %q not found!", torrents)
		}

		val := bucket.Get(key)
		fmt.Println(string(val))

		return nil
	})

	if err != nil {
		log.Fatal(err)
	}
	return
}
